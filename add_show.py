from queries import create_show


show = {
    "name": "Startup",
    "channel": "Netflix",
    "ratings": [],
    "seasons": 1,
}

new_show_id = create_show(show)

print("the new show's id is " + str(new_show_id))
