

# Welcome to the "Best Shows" app

Our goal here is to run the `list_show_avgs.py` file to output a
ranking of our favorite shows. We'll do this by running
`. ./.env; python list_show_avgs.py` and with support
of our `add_show.py`, `add_ratings.py`, `queries.py`, and
`db_connection.py` files

## .env.sample

Please make a file called `.env` in the root of this project.
Then copy the contents of the sample into `.env`. Then replace
the username, password, and the rest of the string with the
connection string value found in MongoDB.

## db_connection.py

Here we'll establish our connection to our remote MongoDB.

## queries.py

It's a good practice to abstract your queries from your functions
in case you can reuse them across multiple functions.

## add_show.py

This is where our create_show query will be called
with some hardcoded show data.

Call this multiple times with different shows to create
enough data for significance. 

## add_ratings.py

This is where our rate_show_by_id query will be called
with the output show id from the previous file, and some
hardcoded rating data.

Again, run this at least once for each show that you created.

## list_show_avgs.py

Finally, here is where we'll get all the shows, and do some
manual aggregations to output the rankings of the top shows
in our data set.
